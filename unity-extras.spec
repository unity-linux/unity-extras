%define  debug_package %{nil}

Summary:        Korora Extras
Name:           unity-extras
Version:        0.14
Release:        4%{?dist}
Source0:        %{name}-%{version}.tar.xz
License:        GPLv3+
Group:          System Environment/Base
Requires:       unity-release
BuildRequires:  policycoreutils libselinux
Requires(post): dconf

%description
This package contains various files required for Korora
such as pretty bash shell, policykit overrides, vimrc, etc.

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/skel/Desktop
mkdir -p %{buildroot}%{_sysconfdir}/fonts/conf.d
mkdir -p %{buildroot}%{_sysconfdir}/profile.d
mkdir -p %{buildroot}%{_sysconfdir}/sysctl.d
mkdir -p %{buildroot}%{_datadir}/polkit-1/rules.d
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_libdir}/firefox/browser/defaults/preferences
mkdir -p %{buildroot}%{_sysconfdir}/dconf/db/local.d


install -m 644 %{_builddir}/%{name}-%{version}/firefox-unity-default-prefs.js %{buildroot}%{_libdir}/firefox/browser/defaults/preferences/firefox-unity-default-prefs.js
install -m 0755 %{_builddir}/%{name}-%{version}/vimrc %{buildroot}%{_sysconfdir}/skel/.vimrc
install -m 0644 %{_builddir}/%{name}-%{version}/unity.sh %{buildroot}%{_sysconfdir}/profile.d/unity.sh
install -m 0644 %{_builddir}/%{name}-%{version}/dircolors.ansi-universal %{buildroot}%{_datadir}/%{name}/dircolors.ansi-universal
install -m 0644 %{_builddir}/%{name}-%{version}/10-unity-policy.rules %{buildroot}%{_datadir}/polkit-1/rules.d/10-unity-policy.rules
/sbin/restorecon %{buildroot}%{_sharedstatedir}/polkit-1/localauthority/50-local.d/10-unity-overrides.pkla

#Set up system-wide hinting
ln -sf /usr/share/fontconfig/conf.avail/10-autohint.conf %{buildroot}/etc/fonts/conf.d/

# Disable coredumps
ln -sf /dev/null %{buildroot}%{_sysconfdir}/sysctl.d/50-coredump.conf

# plank config
install -m 0644 00_unity_plank %{buildroot}%{_sysconfdir}/dconf/db/local.d/00_unity_plank

%posttrans

%post
#Only do this on first install, not upgrades
#if [ "$1" == "1" ]
#then
  #Create vboxusers group
#  groupadd -r vboxusers 2>/dev/null

  #/sbin/restorecon '/var/lib/polkit-1/localauthority/50-local.d/10-unity-overrides.pkla'

  #Set installonly limit in yum.conf
#  if [ -z "$(grep installonly_limit=2 /etc/yum.conf)" ]
#  then
#    sed -i 's/^installonly_limit=.*/installonly_limit=2/g' /etc/yum.conf
#  fi

#  #Set clean_requirements_on_remove in yum.conf
#  if [ -z "$(grep clean_requirements_on_remove /etc/yum.conf)" ]
#  then
#    sed -i '/^installonly_limit=.*/ a clean_requirements_on_remove=1' /etc/yum.conf
#  else
#    sed -i 's/^clean_requirements_on_remove=.*/clean_requirements_on_remove=1/g' /etc/yum.conf
#  fi
#fi

dconf update

%postun

dconf update

%files
%defattr(-,root,root)
%{_sysconfdir}/skel/.vimrc
%{_sysconfdir}/sysctl.d/50-coredump.conf
%{_sysconfdir}/profile.d/unity.sh
%{_datadir}/%{name}/dircolors.ansi-universal
%{_datadir}/polkit-1/rules.d/10-unity-policy.rules
%{_sysconfdir}/fonts/conf.d/10-autohint.conf
%{_libdir}/firefox/browser/defaults/preferences/firefox-unity-default-prefs.js
%{_sysconfdir}/dconf/db/local.d/00_unity_plank

%changelog
* Thu Aug 30 2018 JMiahMan <JMiahMan@unity-linux.org> 0.14-4
- Remove catfish config and put it in xfce xtras
